﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameOver : MonoBehaviour {

    public delegate void EndGame();
    public static event EndGame endGame;

    void PlayerDiedGameOver()
    {
        if(endGame != null)
        {
            endGame();
        }
        Destroy(gameObject);
    }
    void OnTriggerEnter2D(Collider2D target)
    {
        if(target.tag == "Collector")
        {
            PlayerDiedGameOver();
        }
    }
    void OnCollisionEnter2D(Collision2D target)
    {
        if (target.gameObject.tag == "Enemy")
        {
            PlayerDiedGameOver();
        }
    }
}
