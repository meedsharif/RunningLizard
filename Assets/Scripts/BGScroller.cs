﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BGScroller : MonoBehaviour
{

    public float speed = 0.1f;
    private Vector2 offset = Vector2.zero;
    private Material mat;
    public float speedUp = 0.05f;

	// Use this for initialization
	void Start ()
    {
        mat = GetComponent<Renderer>().material;
        offset = mat.GetTextureOffset("_MainTex");

	}
	
	// Update is called once per frame
	void Update ()
    {
        speedUp++;
        //print(speedUp);
        offset.x += speed * speedUp/900 *  Time.deltaTime;
        mat.SetTextureOffset("_MainTex", offset);
        //print("Speed is  " + offset.x);

	}
}
