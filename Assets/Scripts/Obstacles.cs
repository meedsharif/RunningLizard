﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacles : MonoBehaviour {
    private float speed = -3f;
    private float speedUp = -1f;

    private Rigidbody2D obstacleBody;

	// Use this for initialization
	void Awake () {
        obstacleBody = GetComponent<Rigidbody2D>();
	}
    private void Start()
    {
        speed -= speed * speedUp / 10 * Time.deltaTime;
        if(speed > 5)
        {
            speed = 5;
        }
    }

    // Update is called once per frame
    void Update () {
        if(obstacleBody.velocity.x < 5)
        {
            speed -= speed * speedUp / 50 * Time.deltaTime;
            if (speed > 5)
            {
                speed = 5;
            }
            obstacleBody.velocity = new Vector2(speed, 0);
            print(" velocity is " + obstacleBody.velocity);
        }	
        else if(obstacleBody.velocity.x > 5)
        {
            obstacleBody.velocity = new Vector2(5, 0);
        }
    }
}
