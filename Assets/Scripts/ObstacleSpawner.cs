﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour {
    [SerializeField]
    private GameObject[] obstactle;
    private List<GameObject> obstacleForSpawnning = new List<GameObject>();

	// Use this for initialization
	void Awake () {
        InitializedObstacle();
	}
    private void Start()
    {
        StartCoroutine(spawnRandomObstacle());
    }

    // Update is called once per frame
    void Update () {
		
	}

    void InitializedObstacle()
    {
        int index = 0;
        for(int i=0; i<obstactle.Length * 3; i++)
        {
            GameObject obj = Instantiate(obstactle[index], new Vector3(transform.position.x, transform.position.y, -2), Quaternion.identity) as GameObject;
            obstacleForSpawnning.Add(obj);
            obstacleForSpawnning[i].SetActive(false);

            index++;
            if(index == obstactle.Length)
            {
                index = 0;
            }
        }
    }

    void Suffle()
    {
        for (int i = 0; i<obstacleForSpawnning.Count; i++)
        {
            GameObject temp = obstacleForSpawnning[i];
            int random = Random.Range(i, obstacleForSpawnning.Count);
            obstacleForSpawnning[i] = obstacleForSpawnning[random];
            obstacleForSpawnning[random] = temp;



        }

    }

    IEnumerator spawnRandomObstacle()
    {
        yield return new WaitForSeconds(Random.Range(2.0f, 4.5f));

        int index = Random.Range(0, obstacleForSpawnning.Count);
        while (true)
        {
            if (!obstacleForSpawnning[index].activeInHierarchy)
            {
                obstacleForSpawnning[index].SetActive(true);
                obstacleForSpawnning[index].transform.position = new Vector3(transform.position.x, transform.position.y, -2);
                break;
            }
            else
            {
                index = Random.Range(0, obstacleForSpawnning.Count);
            }
        }

        StartCoroutine(spawnRandomObstacle());

    }
}
