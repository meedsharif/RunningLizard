﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GamePlayController : MonoBehaviour {
    [SerializeField]
    private GameObject pausePanel;

    [SerializeField]
    private Button restartGameButton;

    [SerializeField]
    private Text scoretext, pauseText;

    public Text highScore;

    private float speedUp = 1.2f;
    public int score;
   
    // Use this for initialization
    void Start () {
        scoretext.text = score + "s";
        StartCoroutine(CountScore());
        highScore.text = PlayerPrefs.GetInt("HighScore", 0).ToString();
    }
    void Update()
    {
        
    }
    public void HScore()
    {
        
    }

    IEnumerator CountScore()
    {
        yield return new WaitForSeconds(1f); 
        score++;
        scoretext.text = score + "s";

        if(score > PlayerPrefs.GetInt("HighScore", 0))
        {
            PlayerPrefs.SetInt("HighScore", score);
            highScore.text = score.ToString();
        }
        StartCoroutine(CountScore());
    }


    void OnEnable()
    {
        GameOver.endGame += PlayerDiedEndTheGame;
    }
    void OnDisable()
    {
        GameOver.endGame -= PlayerDiedEndTheGame;
    }

    void PlayerDiedEndTheGame()
    {
        if (!PlayerPrefs.HasKey("Score"))
        {
            PlayerPrefs.SetInt("Score", 0);
        }
        else
        {
            int highScore = PlayerPrefs.GetInt("Score");

            if(highScore < score)
            {
                PlayerPrefs.SetInt("Score", score);
            }
        }
        pauseText.text = "Game Over";
        pausePanel.SetActive(true);
        restartGameButton.onClick.RemoveAllListeners();
        restartGameButton.onClick.AddListener (() => RestartGame());

        Time.timeScale = 0f;
    }
    public void PauseGame()
    {
        Time.timeScale = 0f;
        pausePanel.SetActive(true);
        restartGameButton.onClick.RemoveAllListeners();
        restartGameButton.onClick.AddListener(() => ResumeGame());

    }

    public void ResumeGame()
    {
        Time.timeScale = 1f;
        pausePanel.SetActive(false);
    }

    public void RestartGame()
    {
        Time.timeScale = 1f;
        Application.LoadLevel("01.Game");
    }
    public void GoToMenu()
    {
        Time.timeScale = 1f;
        Application.LoadLevel("00.MainMenu");
    }
}
