﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerJump : MonoBehaviour {

    [SerializeField]
    private AudioClip jumpClip;
    private float JumpForce = 12f, forwardForce = 0f;
    private Rigidbody2D playerBody;
    private bool canJump;
    private Button jumpBtn;

	// Use this for initialization
	void Awake () {
        playerBody = GetComponent<Rigidbody2D>();
        jumpBtn = GameObject.Find("Jump Button").GetComponent<Button>();
        jumpBtn.onClick.AddListener(() => Jump());
    }
	
	// Update is called once per frame
	void Update () {
		if(Mathf.Abs(playerBody.velocity.y) == 0)
        {
            canJump = true;
        }
	}

    public void Jump()
    {
        if (canJump)
        {
            canJump = false;

            AudioSource.PlayClipAtPoint(jumpClip, transform.position);
            if (transform.position.x < 0)
            {
                forwardForce = 1f;
            }
            else
            {
                forwardForce = 0;
            }
            playerBody.velocity = new Vector2(forwardForce, JumpForce);
        }
    } 
}
