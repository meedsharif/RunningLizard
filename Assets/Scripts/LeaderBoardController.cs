﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;

public class LeaderBoardController : MonoBehaviour {
    public static LeaderBoardController instance;
    private const string ID = "CgkI1_CV8o4dEAIQAQ";

    private Button LeaderBoardsBtn;
    // Use this for initialization
    void Awake () {
        MakeSingleton();
        GetTheButton();
	}
    void Start()
    {
        PlayGamesPlatform.Activate();    
    }
    void GetTheButton()
    {

        LeaderBoardsBtn = GameObject.Find("LeaderBoardButton").GetComponent<Button>();
        LeaderBoardsBtn.onClick.RemoveAllListeners();
        LeaderBoardsBtn.onClick.AddListener(() => OpenLeaderBoardScore());

    }

    void OnLevelWasLoaded(int level)
    {
        PostScore();    
    }
    // Update is called once per frame
    void Update () {
		
	}
    void MakeSingleton()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }
    public void OpenLeaderBoardScore()
    {
        if (Social.localUser.authenticated)
        {
            PlayGamesPlatform.Instance.ShowLeaderboardUI(ID);
        }
        else
        {
            Social.localUser.Authenticate((bool success) =>
            {
                
            });
        }
    }
    void PostScore()
    {
        if (Social.localUser.authenticated)
        {
            Social.ReportScore(PlayerPrefs.GetInt("Score"), ID, (bool success) => {

            });
        }
    }
}
